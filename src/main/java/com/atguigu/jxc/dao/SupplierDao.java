package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface SupplierDao {
    List<Supplier> findSupplierList(Integer offSet, Integer rows, String supplierName);



    void updateSupplier(Supplier supplier);

    void addSupplier(Supplier supplier);

    void deleteSupplier(List<Integer> idList);
}
