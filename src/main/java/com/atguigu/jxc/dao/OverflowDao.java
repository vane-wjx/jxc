package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;

public interface OverflowDao {
    Integer saveOverflowList(OverflowList overflowList);

    Integer saveOverflowListGoods(OverflowListGoods overflowListGoods);

    List<OverflowList> list(String sTime, String eTime);

    List<OverflowListGoods> findGoodsListById(Integer overflowListId);

    OverflowList findByDamageNumber(String overflowNumber);
}
