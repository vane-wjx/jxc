package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;

public interface DamageDao {
    

    void saveDamageListGoods(DamageListGoods damageListGoods);

    List<DamageList> list(String sTime, String eTime);

    List<DamageListGoods> goodsListById(Integer damageListId);


    void saveDamageList(DamageList damageList);

    DamageList findByDamageNumber(String damageNumber);
}
