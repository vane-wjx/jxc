package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {
    List<Customer> getCustomerList(@Param("offset") Integer offset,
                                   @Param("limit") Integer limit,
                                   @Param("customerName") String customerName);

    void addCustomer(Customer customer);

    void updateCustomer(Customer customer);

    void deleteCustomer(List<Integer> idsList);
}
