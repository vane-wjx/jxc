package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();

    List<Goods> getInventoryList(int offset, Integer rows, String codeOrName, Integer goodsTypeId);


    List<Goods> listAll(int offset, Integer rows, String goodsName, Integer goodsTypeId);

    void updateGoods(Goods goods);

    void addGoods(Goods goods);

    void deleteGoods(Integer goodsId);

    Goods findGoodsById(Integer goodsId);

    List<Goods> getNoInventoryQuantity(int offSet, Integer rows, String nameOrCode);

    List<Goods> getHasInventoryQuantity(int offSet, Integer rows, String nameOrCode);

    void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    void updateStock(Integer goodsId);

    List<Goods> listAlarms();
}
