package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * 客户列表分页（名称模糊查询）
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @PostMapping(value = "/list")
    public Map<String, Object> getCustomerList(@RequestParam("page") Integer page,
                                               @RequestParam("rows") Integer rows,
                                               @RequestParam(value = "customerName", required = false) String customerName) {
        List<Customer> customerList = customerService.getCustomerList(page, rows, customerName);
        Map<String, Object> result = new HashMap<>();
        result.put("total", customerList.size());
        result.put("rows", customerList);
        return result;
    }


    /**
     * 添加或修改用户信息
     * @param customer
     * @return
     */
    @PostMapping(value = "/save")
    public ServiceVO saveOrUpdateCustomer(Customer customer){
        customerService.saveOrUpdateCustomer(customer);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 批量删除客户
     * @param ids
     * @return
     */
    @PostMapping(value = "/delete")
    public ServiceVO deleteCustomer(String ids){
        customerService.deleteCustomer(ids);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

}
