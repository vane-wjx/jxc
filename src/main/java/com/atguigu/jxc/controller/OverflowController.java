package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/overflowListGoods")
public class OverflowController {

    /**
     * 保存报溢单
     */
    @Autowired
    private OverflowService overflowService;
    @PostMapping(value = "/save")
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr){
        return overflowService.save(overflowList, overflowListGoodsStr);
    }

    /**
     * 查询报溢单
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> list(String  sTime, String  eTime){
        return overflowService.list(sTime, eTime);
    }


    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(Integer overflowListId){
        return overflowService.findGoodsListById(overflowListId);
    }

}
