package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping(value = "/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    /**
     * 分页查询供应商
     *
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @PostMapping(value = "/list")
    public Map<String, Object> findSupplierList(Integer page,
                                                Integer rows,
                                                String supplierName) {
        return supplierService.findSupplierList(page, rows, supplierName);
    }


    /**
     * 供应商添加或修改
     * @param supplier
     * @return
     */
    @PostMapping(value = "/save")
    public ServiceVO saveSupplier(Supplier supplier) {
        // 调用Service层方法保存供应商信息

        supplierService.saveSupplier(supplier);

        // 返回ServiceVO对象

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    /**
     * 删除供应商（支持批量删除）
     *
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO deleteSupplier(String ids) {
        supplierService.deleteSupplier(ids);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

}
