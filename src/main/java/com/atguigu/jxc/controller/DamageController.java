package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageController {

    @Autowired
    private DamageService damageService;

    /**
     * 保存报损单
     *
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(DamageList damageList, String damageListGoodsStr) {
        return damageService.save(damageList, damageListGoodsStr);
    }

    /**
     * 查询报损单
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> list(String  sTime, String  eTime){
        return damageService.list(sTime, eTime);
    }

    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        return damageService.goodsListById(damageListId);
    }
}
