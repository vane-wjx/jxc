package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;
    @Override
    public List<Customer> getCustomerList(Integer page, Integer rows, String customerName) {
        page = page == 0 ? 1 : page;
        int offset = (page - 1) * rows;
        List<Customer> customerList = new ArrayList<>();
        customerList =   customerDao.getCustomerList(offset, rows, customerName);
        return customerList;
    }

    @Override
    public void saveOrUpdateCustomer(Customer customer) {

        if (customer.getCustomerId() == null) {
            //添加客户
            customerDao.addCustomer(customer);
        }else {
            //修改客户
            customerDao.updateCustomer(customer);
        }
    }

    @Override
    public void deleteCustomer(String ids) {

        String[] idsArray = ids.split(",");
        List<Integer> idsList = new ArrayList<>();
        for (String id : idsArray) {
            idsList.add(Integer.parseInt(id));
        }
        customerDao.deleteCustomer(idsList);
    }
}
