package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DamageServiceImpl implements DamageService {
    @Autowired
    private DamageDao damageDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO save(DamageList damageList, String damageListGoodsStr) {
        Gson gson = new Gson();

        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>() {
        }.getType());

        // 设置当前操作用户
        User currentUser = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());

        damageList.setUserId(currentUser.getUserId());

        // 保存商品报损单信息
         damageDao.saveDamageList(damageList);

         //根据DamageNumber查询damageListId
        String damageNumber = damageList.getDamageNumber();
        DamageList damageList1 = damageDao.findByDamageNumber(damageNumber);
        Integer damageListId = damageList1.getDamageListId();

        // 保存商品报损单商品信息
        for (DamageListGoods damageListGoods : damageListGoodsList) {
            damageListGoods.setDamageListId(damageListId);

            damageDao.saveDamageListGoods(damageListGoods);

            // 修改商品库存，状态
            Goods goods = goodsDao.findGoodsById(damageListGoods.getGoodsId());

            goods.setInventoryQuantity(goods.getInventoryQuantity() - damageListGoods.getGoodsNum());

            goods.setState(2);

            goodsDao.updateGoods(goods);
        }

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {

        List<DamageList> damageList = damageDao.list(sTime, eTime);
        Map<String, Object> result = new HashMap<>();
        result.put("rows", damageList);
        return result;
    }

    @Override
    public Map<String, Object> goodsListById(Integer damageListId) {
        List<DamageListGoods> damageListGoodsList = damageDao.goodsListById(damageListId);
        Map<String, Object> result = new HashMap<>();
        result.put("rows", damageListGoodsList);
        return result;
    }
}
