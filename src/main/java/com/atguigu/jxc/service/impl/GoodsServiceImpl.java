package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;


    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public List<Goods> getInventoryList(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        page = page == 0 ? 1 : page;
        int offset = (page - 1) * rows;
        List<Goods> goodsList = new ArrayList<>();
        goodsList = goodsDao.getInventoryList(offset, rows, codeOrName, goodsTypeId);
        return goodsList;
    }

    @Override
    public List<Goods> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        page = page == 0 ? 1 : page;
        int offset = (page - 1) * rows;
        List<Goods> goodsList = new ArrayList<>();
        goodsList = goodsDao.listAll(offset, rows, goodsName, goodsTypeId);
        return goodsList;
    }

    @Override
    public void saveOrUpdate(Goods goods) {
        if (goods.getGoodsId() != null) {
            //修改操作
            goodsDao.updateGoods(goods);
        } else {
            //添加操作
            // 设置上一次采购价为当前采购价
            goods.setLastPurchasingPrice(goods.getPurchasingPrice());
            goods.setInventoryQuantity(0);
            goods.setState(0);
            goodsDao.addGoods(goods);
        }
    }

    @Override
    public ServiceVO delete(Integer goodsId) {
        Goods goods = goodsDao.findGoodsById(goodsId);
        Integer state = goods.getState();
        if (state == 1) {
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
        } else if (state == 2) {
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);
        } else {
            goodsDao.deleteGoods(goodsId);
        }

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> noInventoryQuantity = goodsDao.getNoInventoryQuantity(offSet, rows, nameOrCode);
        // 构造返回结果

        Map<String, Object> result = new HashMap<>();
        result.put("total", noInventoryQuantity.size());
        result.put("rows", noInventoryQuantity);
        return result;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> hasInventoryQuantity = goodsDao.getHasInventoryQuantity(offSet, rows, nameOrCode);
        // 构造返回结果

        Map<String, Object> result = new HashMap<>();
        result.put("total", hasInventoryQuantity.size());
        result.put("rows", hasInventoryQuantity);
        return result;
    }

    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.saveStock(goodsId, inventoryQuantity,purchasingPrice);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        Goods goods = goodsDao.findGoodsById(goodsId);
        Integer state = goods.getState();
        if (state == 1) {
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
        } else if (state == 2) {
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);
        } else {
            goodsDao.updateStock(goodsId);
        }

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> listAlarm() {
        List<Goods> goodsList = goodsDao.listAlarms();
        // 构造返回结果

        Map<String, Object> result = new HashMap<>();
        result.put("rows", goodsList);
        return result;
    }


}
