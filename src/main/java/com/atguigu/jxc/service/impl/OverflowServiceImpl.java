package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.OverflowDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowServiceImpl implements OverflowService {
    @Autowired
    private OverflowDao overflowDao;
    @Autowired
    private UserDao userDao;

    @Autowired

    private GoodsDao goodsDao;

    @Override
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr) {
        Gson gson = new Gson();

        List<OverflowListGoods> overflowListGoodsList = gson.fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>() {
        }.getType());

        // 设置当前操作用户
        User currentUser = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());

        overflowList.setUserId(currentUser.getUserId());

        // 保存商品报溢单信息
        overflowDao.saveOverflowList(overflowList);

        //根据DamageNumber查询damageListId
        String overflowNumber = overflowList.getOverflowNumber();
        OverflowList overflowList1 = overflowDao.findByDamageNumber(overflowNumber);
        Integer overflowListId = overflowList1.getOverflowListId();
        // 保存商品报溢单商品信息
        for (OverflowListGoods overflowListGoods : overflowListGoodsList) {
            overflowListGoods.setOverflowListId(overflowListId);

            overflowDao.saveOverflowListGoods( overflowListGoods);

            // 修改商品库存，状态
            Goods goods = goodsDao.findGoodsById(overflowListGoods.getGoodsId());

            goods.setInventoryQuantity(goods.getInventoryQuantity() + overflowListGoods.getGoodsNum());

            goods.setState(2);

            goodsDao.updateGoods(goods);
        }

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        List<OverflowList> overflowLists = overflowDao.list(sTime, eTime);
        Map<String, Object> result = new HashMap<>();
        result.put("rows", overflowLists);
        return result;
    }

    @Override
    public Map<String, Object> findGoodsListById(Integer overflowListId) {
        List<OverflowListGoods> overflowListGoodsList = overflowDao.findGoodsListById(overflowListId);
        Map<String, Object> result = new HashMap<>();
        result.put("rows", overflowListGoodsList);
        return result;
    }
}
