package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;

    @Override
    public Map<String, Object> findSupplierList(Integer page, Integer rows, String supplierName) {
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Supplier> suppliers = supplierDao.findSupplierList(offSet, rows, supplierName);
        // 构造返回结果

        Map<String, Object> result = new HashMap<>();
        result.put("total", suppliers.size());
        result.put("rows", suppliers);

        return result;
    }

    @Override
    public void saveSupplier(Supplier supplier) {

        // 设置供应商ID
        if (supplier.getSupplierId() != null) {
            //修改操作
            supplierDao.updateSupplier(supplier);
        }else {
            //添加操作
            supplierDao.addSupplier(supplier);
        }
    }

    @Override
    public void deleteSupplier(String ids) {

        String[] idArray = ids.split(",");
        List<Integer> idList = new ArrayList<>();
        for (String id : idArray) {
            idList.add(Integer.parseInt(id));
        }
        supplierDao.deleteSupplier(idList);
    }
}
