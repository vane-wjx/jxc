package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.List;
import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    List<Goods> getInventoryList(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    List<Goods> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    void saveOrUpdate(Goods goods);


    ServiceVO delete(Integer goodsId);

    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    ServiceVO deleteStock(Integer goodsId);

    Map<String, Object> listAlarm();
}
