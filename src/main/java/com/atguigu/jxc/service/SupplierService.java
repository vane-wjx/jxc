package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    Map<String, Object> findSupplierList(Integer page, Integer rows, String supplierName);


    void saveSupplier(Supplier supplier);

    void deleteSupplier(String ids);
}
